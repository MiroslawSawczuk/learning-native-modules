package com.nativemodules

import android.annotation.SuppressLint
import com.facebook.react.bridge.*
import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter
import java.util.*

class BluetoothModule(reactContext: ReactApplicationContext) :
    ReactContextBaseJavaModule(reactContext),
    BluetoothScanner.ScanListener,
    BluetoothScanner.ConnectionListener {
  private val scanner = BluetoothScanner(reactContext, this, this)

  override fun getName(): String {
    return "BluetoothModule"
  }

  @ReactMethod
  fun addListener(type: String?) {
    // Keep: Required for RN built in Event Emitter Calls.
  }

  @ReactMethod
  fun removeListeners(type: Int?) {
    // Keep: Required for RN built in Event Emitter Calls.
  }

  @ReactMethod
  fun startScan(promise: Promise) {
    val result = scanner.startScan()
    promise.resolve(result)
  }

  @ReactMethod
  fun stopScan() {
    scanner.stopScan()
  }

  @ReactMethod
  fun connect(address: String) {
    scanner.connect(address)
  }

  @ReactMethod
  fun disconnect(address: String) {
    scanner.disconnect(address)
  }

  @ReactMethod
  fun discoverServices(address: String) {
    scanner.discoverServices(address)
  }

  @ReactMethod
  fun readCharacteristic(address: String, service: String, characteristic: String) {
    scanner.readCharacteristic(address, UUID.fromString(service), UUID.fromString(characteristic))
  }

  @SuppressLint("MissingPermission")
  override fun onScanSuccess(result: WritableNativeMap) {
    reactApplicationContext
        .getJSModule(RCTDeviceEventEmitter::class.java)
        .emit("onScanSuccess", result)
  }

  @SuppressLint("MissingPermission")
  override fun onScanFailed(errorCode: Int) {
    reactApplicationContext
        .getJSModule(RCTDeviceEventEmitter::class.java)
        .emit("onScanFailed", errorCode)
  }

  @SuppressLint("MissingPermission")
  override fun onConnected(result: WritableNativeMap) {
    reactApplicationContext
        .getJSModule(RCTDeviceEventEmitter::class.java)
        .emit("onConnected", result)
  }

  @SuppressLint("MissingPermission")
  override fun onDisconnected(address: String) {
    reactApplicationContext
        .getJSModule(RCTDeviceEventEmitter::class.java)
        .emit("onDisconnected", address)
  }

  @SuppressLint("MissingPermission")
  override fun onServicesDiscovered(services: WritableNativeMap) {
    reactApplicationContext
        .getJSModule(RCTDeviceEventEmitter::class.java)
        .emit("onServicesDiscovered", services)
  }
}
