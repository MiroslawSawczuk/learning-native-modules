package com.nativemodules

import android.annotation.SuppressLint
import android.bluetooth.*
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.facebook.react.bridge.WritableNativeArray
import com.facebook.react.bridge.WritableNativeMap
import java.util.*

@SuppressLint("MissingPermission")
class BluetoothScanner(
  private val context: Context,
  private val scanListener: ScanListener,
  private val connectionListener: ConnectionListener
) {
  private val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
  private var bluetoothLeScanner: BluetoothLeScanner? = null
  private val handler = Handler(Looper.getMainLooper())
  private val bleGattMap: MutableMap<String, BluetoothGatt> = mutableMapOf()

  fun startScan(): Boolean {
    return try {
      if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled) {
        false
      } else {
        val scanSettings =
          ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_POWER).build()

        bluetoothLeScanner = bluetoothAdapter.bluetoothLeScanner
        bluetoothLeScanner?.startScan(null, scanSettings, scanCallback)
        true
      }
    } catch (ex: Exception) {
      Log.d(TAG, "Error startScan: $ex")
      false
    }
  }

  fun stopScan() {
    try {
      bluetoothLeScanner?.stopScan(scanCallback)
    } catch (ex: Exception) {
      Log.d(TAG, "Error stopScan: $ex")
    }
  }

  fun connect(address: String) {
    try {
      val device = bluetoothAdapter.getRemoteDevice(address)

      if (device.bondState == BluetoothDevice.BOND_NONE) {
        device.createBond()
      } else {
        val gatt = device.connectGatt(context, true, gattCallback)
        gatt?.requestMtu(517)
        bleGattMap[address] = gatt
      }
    } catch (ex: Exception) {
      Log.d(TAG, "Error connect: $ex")
    }
  }

  fun disconnect(address: String) {
    try {
      val device = bluetoothAdapter.getRemoteDevice(address)

      if (device.bondState == BluetoothDevice.BOND_BONDED) {
        val gatt = bleGattMap[address]
        gatt?.disconnect()
      }
    } catch (ex: Exception) {
      Log.d(TAG, "Error disconnect: $ex")
    }
  }

  fun discoverServices(address: String) {
    try {
      val device = bluetoothAdapter.getRemoteDevice(address)

      if (device.bondState == BluetoothDevice.BOND_BONDED) {
        val gatt = bleGattMap[address]
        gatt?.discoverServices()
      }
    } catch (ex: Exception) {
      Log.d(TAG, "Error discoverServices: $ex")
    }
  }

  fun readCharacteristic(address: String, serviceUUID: UUID, characteristicUUID: UUID) {
    try {
      val device = bluetoothAdapter.getRemoteDevice(address)

      if (device.bondState == BluetoothDevice.BOND_BONDED) {
        val gatt = bleGattMap[address]
        val service = gatt?.getService(serviceUUID)
        val characteristic = service?.getCharacteristic(characteristicUUID)

        val RX = UUID.fromString("6e400002-b5a3-f393-e0a9-e50e24dcca9e")

        if (characteristicUUID == RX) {
          val rxCharacteristic = service?.getCharacteristic(characteristicUUID)
          if (rxCharacteristic != null) {
            val command = byteArrayOf(0x01, 0x00)
            rxCharacteristic.value = command
            gatt.writeCharacteristic(rxCharacteristic)

            Log.d(TAG, "RX characteristic write start")
          }
        } else {
          gatt?.setCharacteristicNotification(characteristic, true)
          gatt?.readCharacteristic(characteristic)
        }
      }
    } catch (ex: Exception) {
      Log.d(TAG, "Error readCharacteristic: $ex")
    }
  }

  private val scanCallback =
    object : ScanCallback() {
      override fun onScanResult(callbackType: Int, scanResult: ScanResult) {
        val device = scanResult.device
        val result = WritableNativeMap()
        val params = WritableNativeMap()

        params.putString("name", device.name)
        params.putInt("bondState", device.bondState)

        result.putMap(device.address, params)
        handler.post { scanListener.onScanSuccess(result) }
      }

      override fun onScanFailed(errorCode: Int) {
        handler.post { scanListener.onScanFailed(errorCode) }
      }
    }

  private val gattCallback =
    object : BluetoothGattCallback() {
      override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
        val device = gatt.device
        val address = gatt.device.address

        if (status == BluetoothGatt.GATT_SUCCESS) {
          if (newState == BluetoothProfile.STATE_CONNECTED) {
            Log.d(TAG, "Device connected $address")

            val result = getConnectedDeviceReactResult(device)
            connectionListener?.onConnected(result)
          } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
            Log.d(TAG, "Device disconnected $address")
            bleGattMap.remove(address)
            gatt.close()

            connectionListener?.onDisconnected(address)
          }
        } else {
          bleGattMap.remove(device.address)
          gatt.close()
        }
      }

      override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
        val address = gatt?.device?.address
        val services = gatt?.services

        if (status == BluetoothGatt.GATT_SUCCESS) {
          if (address !== null && services?.isNotEmpty()!!) {
            val result = getDiscoveredServicesReactResult(address, services)
            connectionListener?.onServicesDiscovered(result)

            // Print services only
            printGattTable(services)
          }
        }
      }

      override fun onCharacteristicRead(
        gatt: BluetoothGatt?,
        characteristic: BluetoothGattCharacteristic?,
        status: Int
      ) {
        Log.d(TAG, "onCharacteristicRead")
        if (status == BluetoothGatt.GATT_SUCCESS) {
          readValue(characteristic)
        }
      }

      override fun onCharacteristicWrite(
        gatt: BluetoothGatt?,
        characteristic: BluetoothGattCharacteristic?,
        status: Int
      ) {
        Log.d(TAG, "onCharacteristicWrite $status")
        if (status == BluetoothGatt.GATT_SUCCESS) {
          val hexValue = characteristic?.value?.toHexString()
          Log.d(TAG, "Characteristic write success: $hexValue")
          Log.d(TAG, "Characteristic write success: ${characteristic?.value}")

        } else {
          Log.e(TAG, "Characteristic write failed with status: $status")
        }
      }

      override fun onCharacteristicChanged(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic) {
        Log.d(TAG, "onCharacteristicChanged ")
        readValue(characteristic)
      }

      // TODO implement onBondStateChanged to fire event to react app that device is bonded
    }


  fun ByteArray.toHexString(): String = joinToString(separator = " ", prefix = "0x") { String.format("%02X", it) }

  fun readValue(characteristic: BluetoothGattCharacteristic?) {
    val hexValue = characteristic?.value?.toHexString()
    Log.d(TAG, "hexValue $hexValue")

    if (!hexValue.isNullOrEmpty()) {
      val bytes = hexValue.substring(2).split(" ").map { it.toInt(16).toByte() }
        .toByteArray()

      if (bytes.isNotEmpty()) {
        val stringValue = String(bytes, Charsets.UTF_8)
        Log.d(TAG, "bytes $bytes")
        Log.d(TAG, "String value $stringValue")

      }
    }
  }

  fun printGattTable(services: List<BluetoothGattService>) {
    services.forEach { service ->
      val characteristicsTable =
        service.characteristics.joinToString(separator = "\n|--", prefix = "|--") {
          it.uuid.toString()
        }
      Log.v(TAG, "\nService ${service.uuid}\nCharacteristics:\n$characteristicsTable")
    }
  }

  fun getConnectedDeviceReactResult(device: BluetoothDevice): WritableNativeMap {
    val result = WritableNativeMap()
    val params = WritableNativeMap()
    params.putString("name", device.name)
    params.putInt("bondState", device.bondState)
    result.putMap(device.address, params)
    return result
  }

  fun getDiscoveredServicesReactResult(
    address: String?,
    services: List<BluetoothGattService>
  ): WritableNativeMap {
    val servicesArray =
      services.map { service ->

        // create characteristic Array
        val characteristicsArray =
          service.characteristics?.map { characteristic ->
            val characteristicData = WritableNativeMap()
            characteristicData.putString("uuid", characteristic.uuid.toString())
            characteristicData
          }

        // create single service object with characteristics
        val serviceData = WritableNativeMap()
        serviceData.putString("uuid", service.uuid.toString())
        serviceData.putArray(
          "characteristics",
          WritableNativeArray().apply {
            characteristicsArray?.forEach { characteristicData -> pushMap(characteristicData) }
          }
        )

        serviceData
      }

    val servicesData = WritableNativeArray()
    servicesArray?.forEach { serviceData -> servicesData.pushMap(serviceData) }

    val result = WritableNativeMap()
    result.putString("address", address)
    result.putArray("services", servicesData)
    return result
  }

  interface ScanListener {
    fun onScanSuccess(result: WritableNativeMap)
    fun onScanFailed(errorCode: Int)
  }

  interface ConnectionListener {
    fun onConnected(result: WritableNativeMap)
    fun onDisconnected(address: String)
    fun onServicesDiscovered(services: WritableNativeMap)
  }

  companion object {
    private const val TAG = "BluetoothScanner"
  }
}
