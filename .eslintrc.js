module.exports = {
  root: true,
  extends: [
    'eslint:recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
    'airbnb',
    'airbnb/hooks',
    'plugin:prettier/recommended',
    'prettier/react',
  ],
  rules: {
    'no-use-before-define': 'off',
    'react/function-component-definition': [
      2,
      {
        namedComponents: 'arrow-function',
        unnamedComponents: 'arrow-function',
      },
    ],
    'react/prop-types': 0,
    'import/extensions': 0,
    'react/jsx-props-no-spreading': 0,
    'import/prefer-default-export': 0,
    'import/no-unresolved': 0 /* FIXME: temporary disabled due to problem with webpack aliases configuration in Next.js webpack config */,
    'react/jsx-filename-extension': [1, { extensions: ['.tsx', '.jsx'] }],
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    'arrow-body-style': ['error', 'as-needed'],
    'jsx-a11y/anchor-is-valid': 0,
    'global-require': 0,
    'jsx-a11y/label-has-associated-control': [
      2,
      {
        controlComponents: ['Input'],
      },
    ],
    'react/require-default-props': 0,
    'no-shadow': 'off', // replaced by ts-eslint rule below
    '@typescript-eslint/no-shadow': 'error',
    'react/style-prop-object': [
      'error',
      {
        allow: ['StatusBar'],
      },
    ],
    'react/jsx-key': 'error',
  },
  overrides: [
    {
      files: ['**/*.ts', '**/*.tsx'],
      parser: '@typescript-eslint/parser',
      parserOptions: {
        project: 'tsconfig.json',
      },
      plugins: ['@typescript-eslint'],
      extends: [
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:import/typescript',
        'prettier/@typescript-eslint',
      ],
      rules: {
        'no-console': 1,
        'no-await-in-loop': 0,
        'react-hooks/exhaustive-deps': 0,
        'no-unused-expressions': 0,
        'react/no-unstable-nested-components': 0,
        '@typescript-eslint/ban-ts-comment': 'warn',
        'spaced-comment': ['error', 'always', { markers: ['/'] }] /* Enable TypeScript's Triple-Slash Directives */,
        '@typescript-eslint/no-unused-vars': [
          'error',
          {
            argsIgnorePattern: '^_+',
            ignoreRestSiblings: true,
            varsIgnorePattern: '^_+',
            args: 'none',
          },
        ],
      },
    },
  ],
};
