import Snackbar from "react-native-snackbar";

export const showSnackbar = (msg: string) => {
  Snackbar.show({
    text: msg,
    duration: Snackbar.LENGTH_SHORT,
    marginBottom: 30
  });
}