import { useEffect, useState } from "react";
import { EVENTS } from '../types/events.types';
import { NativeEventEmitter, NativeModules } from "react-native";
import { DeviceServices } from "../types/useService.types";
const { BluetoothModule } = NativeModules;
const { discoverServices, readCharacteristic } = BluetoothModule;

export const useService = () => {
  const eventEmitter = new NativeEventEmitter(BluetoothModule);

  const [devicesServices, setDevicesServices] = useState<DeviceServices[] | undefined>()

  const discoverServicesForDevice = (address: string) => {
    try {
      discoverServices(address);
    }
    catch (ex) {
      console.log(`Error during discoverServicesForDevice: ${ex}`);
    }
  }
  const readCharacteristicFromDevice = (address: string, service: string, characteristic: string) => {
    try {
      readCharacteristic(address, service, characteristic);
    }
    catch (ex) {
      console.log(`Error during discoverServicesForDevice: ${ex}`);
    }
  }

  useEffect(() => {
    const onServicesDiscoveredSubscription = eventEmitter.addListener(EVENTS.onServicesDiscovered,
      (deviceServices: DeviceServices) => handleOnServicesDiscovered(deviceServices));

    return () => {
      onServicesDiscoveredSubscription.remove();
    };
  }, []);

  const handleOnServicesDiscovered = (deviceServices: DeviceServices) => {
    setDevicesServices(prevState => {
      if (prevState) {
        const filtered = prevState.filter(a => a.address !== deviceServices.address)
        filtered.push(deviceServices)
        return filtered
      } else {
        return [deviceServices]
      }
    })
  }

  return {
    discoverServicesForDevice,
    devicesServices,
    readCharacteristicFromDevice
  };
};
