import { useEffect, useState } from "react";
import { ConnectedDeviceMap, ScannedDeviceMap } from '../types/useDevice.types';
import { EVENTS } from '../types/events.types';
import { NativeEventEmitter, NativeModules } from "react-native";
import { requestBleOn, requestBlePermissions } from "./usePermission";
import { showSnackbar } from "../utils/snackbar";
const { BluetoothModule } = NativeModules;
const { startScan, stopScan, connect, disconnect } = BluetoothModule;

export const useDevice = () => {
  const eventEmitter = new NativeEventEmitter(BluetoothModule);

  const [isScanning, setIsScanning] = useState(false);
  const [scannedDevices, setScannedDevices] = useState<ScannedDeviceMap | undefined>();
  const [connectedDevices, setConnectedDevices] = useState<ConnectedDeviceMap | undefined>();


  useEffect(() => {
    if (isScanning) {
      const onScanSuccessSubscription = eventEmitter.addListener(EVENTS.onScanSuccess,
        (device: ScannedDeviceMap) => handleOnScanSuccess(device));

      const onScanFailedSubscription = eventEmitter.addListener(EVENTS.onScanFailed,
        error => console.log(`An error has occured during scanning: ${error}`));

      const onConnectedSubscription = eventEmitter.addListener(EVENTS.onConnected,
        (device: ConnectedDeviceMap) => handleOnConnected(device));

      const onDisconnectedSubscription = eventEmitter.addListener(EVENTS.onDisconnected,
        (address: string) => handleOnDisconnected(address));

      return () => {
        onScanSuccessSubscription.remove();
        onScanFailedSubscription.remove();
        onConnectedSubscription.remove();
        onDisconnectedSubscription.remove();
      };
    }
  }, [isScanning]);


  const startScanning = async () => {
    try {
      requestBlePermissions(isGranted => {
        if (isGranted) {
          requestBleOn(async isOn => {
            if (isOn) {
              const isScanStarted = await startScan();
              if (isScanStarted) {
                setIsScanning(true);
              }
            } else {
              showSnackbar('Please turn on bluetooth');
            }
          })
        } else {
          showSnackbar('Please grant bluetooth permissions');
        }
      });
    }
    catch (ex) {
      showSnackbar('Ops! An error occured during scanning');
      console.log(`Error during Start scanning: ${ex}`)
    }
  }

  const stopScanning = () => {
    try {
      stopScan();
      setScannedDevices(undefined)
      setIsScanning(false);
    }
    catch (ex) {
      console.log(`Error during Stop scanning: ${ex}`)
    }
  }

  const connectToDevice = (address: string) => {
    try {
      connect(address);
    }
    catch (ex) {
      console.log(`Error during connectToDevice: ${ex}`)
    }
  }

  const disconnectFromDevice = (address: string) => {
    try {
      disconnect(address);
    }
    catch (ex) {
      console.log(`Error during disconnectFromDevice: ${ex}`)
    }
  }

  const handleOnScanSuccess = (device: ScannedDeviceMap) => {
    setScannedDevices(prevState => ({
      ...prevState,
      ...device,
    }));
  };

  const handleOnConnected = async (device: ConnectedDeviceMap) => {
    setConnectedDevices(prevState => ({
      ...prevState,
      ...device,
    }));

    setScannedDevices(prevState => {
      if (prevState) {
        const address = Object.keys(device)[0];
        const devices = { ...prevState };  // the same like this const filteredScannedDevices: ScannedDevice = Object.fromEntries(filteredDevices);
        delete devices[address];
        return devices;
      }
    });
  };

  const handleOnDisconnected = (address: string) => {
    setConnectedDevices(prevState => {
      if (prevState) {
        const devices = { ...prevState };
        delete devices[address];
        return devices;
      }
    });
  }

  return {
    startScanning,
    stopScanning,
    scannedDevices,
    isScanning,
    connectToDevice,
    disconnectFromDevice,
    connectedDevices
  };
};
