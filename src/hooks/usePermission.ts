import { requestMultiple, PERMISSIONS } from "react-native-permissions";
import { Platform, PermissionsAndroid } from "react-native";
import BluetoothStateManager from 'react-native-bluetooth-state-manager';
import DeviceInfo from "react-native-device-info";

type BooleanCallback = (result: boolean) => void;

export const requestBlePermissions = async (cb: BooleanCallback) => {
  try {
    if (Platform.OS === 'ios') {
      cb(true);
    } else {
      const apiLevel = await DeviceInfo.getApiLevel();

      if (apiLevel < 31) {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Location Permission',
            message: 'Bluetooth Low Energy requires Location',
            buttonNeutral: 'Ask Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        cb(granted === PermissionsAndroid.RESULTS.GRANTED);
      } else {
        const result = await requestMultiple([
          PERMISSIONS.ANDROID.BLUETOOTH_SCAN,
          PERMISSIONS.ANDROID.BLUETOOTH_CONNECT,
          PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
        ]);

        const isGranted =
          result['android.permission.BLUETOOTH_SCAN'] ===
          PermissionsAndroid.RESULTS.GRANTED &&
          result['android.permission.BLUETOOTH_CONNECT'] ===
          PermissionsAndroid.RESULTS.GRANTED &&
          result['android.permission.ACCESS_FINE_LOCATION'] ===
          PermissionsAndroid.RESULTS.GRANTED;

        cb(isGranted);
      }
    }
  }
  catch (ex) {
    console.log(ex)
    cb(false)
  }
};

export const requestBleOn = async (cb: BooleanCallback) => {
  try {
    const bleState = await BluetoothStateManager.getState();

    if (bleState === 'PoweredOn') {
      cb(true)
    } else {
      if (Platform.OS === 'ios') {
        await BluetoothStateManager.openSettings();
      } else {
        await BluetoothStateManager.requestToEnable()

        const bleState = await BluetoothStateManager.getState();
        if (bleState === 'PoweredOn') {
          cb(true)
        } else {
          await BluetoothStateManager.openSettings();
        }
      }
    }
  }
  catch (ex) {
    console.log(ex)
    cb(false)
  }
};