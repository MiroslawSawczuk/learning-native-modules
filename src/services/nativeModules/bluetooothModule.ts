import { NativeModules } from 'react-native';

const { BluetoothModule } = NativeModules;

export const startScan = async (): Promise<boolean> => BluetoothModule.startScan();
export const stopScan = (): void => BluetoothModule.stopScan();
export const connect = (address: string): void => BluetoothModule.connect(address);
export const disconnect = (address: string): void => BluetoothModule.disconnect(address);
export const discoverServices = (address: string): void => BluetoothModule.discoverServices(address);
export const readCharacteristic = (address: string, service: string, characteristic: string): void => BluetoothModule.readCharacteristic(address, service, characteristic);
