import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { useDevice } from '../hooks/useDevice'
import { useService } from '../hooks/useService';

const Home = () => {
  const { startScanning, stopScanning, scannedDevices, connectToDevice, connectedDevices, disconnectFromDevice } = useDevice()
  const { discoverServicesForDevice, devicesServices, readCharacteristicFromDevice } = useService();
  // TODO | tutaj lub w hooku useDevice powinno nastąpić zapisanie connectedDevices to Store
  // TODO | aby w innych ekranach móc z nich korzystać i tym samym zamknąć listenery na scanowanie urządzeń


  const discover = () => {
    if (!!connectedDevices) {
      discoverServicesForDevice(Object.keys(connectedDevices)[0])
    }
  }

  const ConnectedDevices = () => (
    connectedDevices ?
      <View style={{ marginBottom: 20, borderBottomWidth: 1 }}>
        <Text>CONNECTED DEVICES</Text>
        {Object.keys(connectedDevices).map((device) =>
          <TouchableOpacity key={device} style={{ marginBottom: 10 }} onPress={() => disconnectFromDevice(device)}>

            <View style={{ flexDirection: 'row' }} >
              <Text>{connectedDevices[device].name || 'unknown'}</Text>
            </View>
          </TouchableOpacity>
        )}
      </View>
      : <></>
  )

  const ScannedDevices = () => (
    scannedDevices ?
      <View>
        {Object.keys(scannedDevices).map((device) =>
          <TouchableOpacity key={device} style={{ marginBottom: 10 }} onPress={() => connectToDevice(device)}>

            <View style={{ flexDirection: 'row' }} >
              <Text>{device}</Text>
              <Text>{scannedDevices[device].name || 'unknown'}</Text>
            </View>
          </TouchableOpacity>
        )}
      </View>
      : <></>
  )

  const DevicesServices = () => (
    devicesServices ?
      <View>
        {devicesServices.map(d =>
          <View key={d.address}>
            <Text style={{ marginBottom: 5, fontSize: 20 }}>device address: {d.address}</Text>
            <View>
              {d.services.map(s => (
                <View key={s.uuid} style={{ marginLeft: 10 }} >
                  <Text style={{ fontWeight: '600', marginTop: 5 }}>Service uuid: {s.uuid}</Text>
                  <View>
                    {s.characteristics.map(c => (
                      <TouchableOpacity onPress={() => readCharacteristicFromDevice(d.address, s.uuid, c.uuid)} key={c.uuid} style={{ marginLeft: 20 }}>
                        <Text >Characteristic uuid: {c.uuid}</Text>
                      </TouchableOpacity>
                    ))}
                  </View>
                </View>
              ))}
            </View>
          </View>
        )}
      </View>
      : <></>
  )

  return (
    <SafeAreaView style={styles.safeArea}>
      <ScrollView style={styles.container} contentContainerStyle={{ paddingBottom: 30 }}>
        <TouchableOpacity onPress={() => startScanning()} style={styles.btn}>
          <Text>startScanning</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => stopScanning()} style={styles.btn}>
          <Text>stopScan</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => discover()} style={styles.btn}>
          <Text>discover services</Text>
        </TouchableOpacity>

        <ConnectedDevices />
        <ScannedDevices />
        <DevicesServices />
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
  },
  container: {
    flex: 1,
    padding: 10,
  },
  btn: {
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,

    borderWidth: 1,
    borderColor: 'black',
  },
});

export default Home;
function readCharacteristicFromDevice(address: string, uuid: string, uuid1: string): ((event: import("react-native").GestureResponderEvent) => void) | undefined {
  throw new Error('Function not implemented.');
}

