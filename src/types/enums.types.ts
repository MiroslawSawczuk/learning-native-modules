export enum BondState {
  None = 10,
  Bonding = 11,
  Bonded = 12,
}