export enum EVENTS {
    onScanSuccess = 'onScanSuccess',
    onScanFailed = 'onScanFailed',
    onConnected = 'onConnected',
    onDisconnected = 'onDisconnected',
    onServicesDiscovered = 'onServicesDiscovered',
}
