import { BondState } from "./enums.types";

export type ScannedDeviceMap = {
    [address: string]: { name: string, bondState: BondState };
};

export type ConnectedDeviceMap = {
    [address: string]: { name: string, bondState: BondState };
};