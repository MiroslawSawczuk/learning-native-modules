export type Characteristic = {
    uuid: string
}

export type Service = {
    uuid: string
    characteristics: Characteristic[]
}

export type DeviceServices = {
    address: string;
    services: Service[];
};